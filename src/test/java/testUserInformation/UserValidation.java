package testUserInformation;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserValidation {
	@FindBy(id="username")
	private static WebElement txt_EmailId;
	@FindBy(id="login-submit")
	private static WebElement btn_Submit;
	@FindBy(id="username")
	private static WebElement txt_UserName;
	@FindBy(id="password")
	private static WebElement txt_Password;
	@FindBy(id="signIn")
	private static WebElement btn_SignIn;
	@FindBy(xpath="//a[text()='Next']")
	private static WebElement lnk_Next;
	

	public static void main(String[] args) throws Exception{

		String filePath = System.getProperty("user.dir") +"/Test.xlsx";
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") +"/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		PageFactory.initElements(driver, UserValidation.class);
		driver.get("https://ftdcorp.atlassian.net/admin");
		txt_EmailId.sendKeys("rvejendla@ftdi.com");
		btn_Submit.click();		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
		txt_UserName.sendKeys("rvejendla");
		txt_Password.sendKeys("Mithun1$");
		btn_SignIn.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Users']")));
		driver.switchTo().frame(0);
	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tbody/tr[1]/td/span")));
		
		int count=0;
		int days=0;
		int nerverLoggedIn =0;
		long diff=0;
		String ValidatingText = "Never logged in";
		HashMap <String, String> userDetails = new HashMap<String, String>();
		try {
			//boolean Next= true;		
			while(count<21) {
				List<WebElement> usernames = driver.findElements(By.xpath("//tr/td/a"));
				List<WebElement> lastActive = driver.findElements(By.xpath("//tr//td/span"));
				List<String> Text_userNames = new ArrayList<String>();
				List<String> Text_lastActive = new ArrayList<String>();
				
				
				for(int i=0; i<usernames.size(); i++) {
					Text_userNames.add(usernames.get(i).getText());
					Text_lastActive.add(lastActive.get(i).getText());
					if(lastActive.get(i).getText().equals(ValidatingText)) {
						nerverLoggedIn++;	
						//need to write code for writing the data in to file
						FileInputStream fis = new FileInputStream(filePath);
						 XSSFWorkbook workbook = new XSSFWorkbook(fis);
					
						 XSSFSheet sheet = workbook.getSheet("NeverLogin");
						 Row row = sheet.createRow(nerverLoggedIn);
						 
						 Cell cell = row.createCell(0);
						
						 cell.setCellValue(usernames.get(i).getText());
						 FileOutputStream fos = new FileOutputStream(filePath);
						 workbook.write(fos);
						 fos.close();
						
					}
					else {
						userDetails.put(usernames.get(i).getText(), lastActive.get(i).getText());
						SimpleDateFormat formatter = new SimpleDateFormat("d MMM yyyy");
				        String dateInString = userDetails.get(usernames.get(i).getText());

				        try {
				        	
				            Date date = formatter.parse(dateInString);				          
				            Date currentDate = new Date();
				            
				            long diffInMillies = Math.abs(currentDate.getTime() - date.getTime());
				    	    diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
				           
				    	    if(diff>30) {
				    	    	days++;
				    	    	
				    	    	FileInputStream fis = new FileInputStream(filePath);
								 XSSFWorkbook workbook = new XSSFWorkbook(fis);								
								 XSSFSheet sheet = workbook.getSheet("Days");
								 Row row = sheet.createRow(days);
								 
								 Cell cell = row.createCell(0);
								 Cell cell1 = row.createCell(1);
								 //cell.setCellType(cell.CELL_TYPE_STRING);
								 cell.setCellValue(usernames.get(i).getText());
								 cell1.setCellValue(lastActive.get(i).getText());
								 FileOutputStream fos = new FileOutputStream(filePath);
								 workbook.write(fos);
								 fos.close();
				    	    }
				    	 
				    		
				        } catch (ParseException e) {
				            e.printStackTrace();
				        }
						
					}
					//System.out.println(usernames.get(i).getText()+"----"+lastActive.get(i).getText()+"-----"+diff);
				}
							
				if(count<20) {
					wait.until(ExpectedConditions.elementToBeClickable(lnk_Next));
					lnk_Next.click();
				}
						
				count++;
				Thread.sleep(3000);
				
			}
		}
		
		catch(NoSuchElementException e) {
			e.printStackTrace();
		}	
		driver.quit();
		
	}

}
